package xyz.novichkov.novichkov.handlebell;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

    TextView tvText;
    SensorManager sensorManager;
    Sensor sensorAccel;
    private MediaPlayer mp;
    private ImageButton mImageButton;

        private View.OnClickListener mOnButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            updateStatus();
        }
    };

    private long lastUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvText = findViewById(R.id.tvText);
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorAccel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mp = MediaPlayer.create(this, R.raw.bell_sound);
        mp.setLooping(true);
        mImageButton = findViewById(R.id.imageButton);
        mImageButton.setOnClickListener(mOnButtonListener);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(listener, sensorAccel,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(listener);
        mp.pause();
    }

    private void updateStatus()
    {
        if (mp.isPlaying())
        {
            mp.pause();
            mImageButton.setImageResource(R.drawable.white_bell);
            tvText.setText(R.string.syop_music_text);
        }
        else
        {
            mp.start();
            mImageButton.setImageResource(R.drawable.red_bell);
            tvText.setText(R.string.play_music_text);
        }
    }


    SensorEventListener listener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    float[] values= event.values;
                    // проекции ускорения на оси системы координат
                    float x= values[0];
                    float y= values[1];
                    float z= values[2];

                    // квадрат модуля ускорения телефона, деленный на квадрат
                    //ускорения свободного падения
                    float accelationSquareRoot=(x* x+ y* y+ z* z)
                            /(SensorManager.GRAVITY_EARTH* SensorManager.GRAVITY_EARTH);
                    //Текущее время
                    long actualTime=System.currentTimeMillis();

                    if(accelationSquareRoot>=2)//Если тряска сильная
                    {
                        if (actualTime - lastUpdate < 2000) {
                            //Если с момента начала тряски прошло меньше 200
                            // миллисекунд - выходим из обработчика
                            return;
                        }
                        lastUpdate = actualTime;
                        updateStatus();
                    }
                    break;
            }
        }
    };

}